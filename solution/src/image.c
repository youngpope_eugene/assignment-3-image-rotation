#include "image.h"

void free_image(struct image img){
    if (img.data != NULL) {
        free(img.data);
        img.data = NULL;
    }
}

struct image allocate_image(uint64_t width, uint64_t height) {
    struct image allocated_image = (struct image) {
            .data = malloc(sizeof(struct pixel) * width * height),
            .width = width,
            .height = height};
    return allocated_image;
}
