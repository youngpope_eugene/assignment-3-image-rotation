#include "rotate.h"

struct image rotate(struct image const* rotating_image){
    struct image rotated_image = allocate_image(rotating_image->height, rotating_image->width);
    if (rotated_image.data == NULL) {
        return rotated_image;
    }
    uint64_t rotating_image_width = rotating_image->width;
    uint64_t rotating_image_height = rotating_image->height;
    for(size_t i = 0; i < rotating_image_height; i++){
        for(size_t j = 0; j < rotating_image_width; j++){

            rotated_image.data[rotated_image.width * j + i] =
                    rotating_image->data[j + rotating_image_width * (rotating_image_height - 1 - i)];

        }
    }
    return rotated_image;
}
