#include "bmp-util.h"
#define BMP_TYPE 0x4D42
#define BMP_SIZE 40
#define BMP_BIT 24

static uint64_t calculate_padding(uint64_t width){
    if(width % 4 != 0) return 4 - (width * (sizeof(struct pixel))) % 4;
    return 0;
}

static enum read_status read_header(bmpHeader* header, FILE* file){
    if (fread(header, sizeof(bmpHeader), 1, file) == 1) {
        if (header->bfType == BMP_TYPE) {
            return READ_OK;
        }
        return WRONG_FILE_READ_ERROR;
    }
    return READ_ERROR;
}

static enum read_status read_data(struct image* img, FILE* file){
    uint64_t padding = calculate_padding(img->width);
    struct pixel* pixels = malloc (sizeof(struct pixel) * img->height * img->width );
    if (pixels == NULL) {
        return READ_OPERATION_ALLOCATION_ERROR;
    }
    size_t fileWorkStatus;
    for(uint64_t i = 0; i < img->height; i++){
        fileWorkStatus = fread(i * img->width + pixels, sizeof(struct pixel), img->width, file);
        if (!fileWorkStatus) {
            free(pixels);
            img->data = NULL;
            return READ_ERROR;
        }
        fileWorkStatus = fseek(file, (long) padding, SEEK_CUR);
        if (fileWorkStatus) {
            free(pixels);
            img->data = NULL;
            return READ_ERROR;
        }
    }
    img->data = pixels;
    return READ_OK;
}

enum read_status from_bmp(struct image* img, FILE* file){
    bmpHeader header = {0};
    enum read_status readStatus = read_header(&header, file);
    if (readStatus != READ_OK) {
        return readStatus;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    return read_data(img, file);
}

enum write_status write_header(struct image* img, FILE* const file){
    uint64_t width = img->width;
    uint64_t height = img->height;
    uint64_t padding = calculate_padding(width);
    const size_t image_size = (sizeof(struct pixel) * (width) + padding) * height;
    bmpHeader header = {
            .biSizeImage = image_size,
            .biClrUsed = 0,
            .bfReserved = 0,
            .biPlanes = 1,
            .biClrImportant = 0,
            .biSize = BMP_SIZE,
            .biWidth = width,
            .bfType = BMP_TYPE,
            .biBitCount = BMP_BIT,
            .biHeight = height,
            .biXPelsPerMeter = 0,
            .biCompression = 0,
            .bfileSize = sizeof(bmpHeader) + image_size,
            .biYPelsPerMeter = 0,
            .bOffBits = sizeof(bmpHeader)
    };

    if (fwrite(&header, sizeof(bmpHeader), 1, file) == 1) return WRITE_OK;
    return WRITE_ERROR;
}

enum write_status write_data(struct image* img, FILE* const file) {
    uint64_t padding = calculate_padding(img->width);
    uint64_t* const paddings[3] = {0};
    for (size_t i = 0; i < img->height; i++){
        if (!fwrite(img->width * i + (img->data), sizeof(struct pixel) * img->width, 1, file)) return WRITE_ERROR;
        if (!fwrite(paddings, padding, 1, file)) return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(struct image* img, FILE* file){
    if (write_header(img, file) != WRITE_OK) return WRITE_ERROR;
    return write_data(img, file);
}







