#include "file.h"

enum close_status close_file(FILE** file) {
    if (fclose(*file) == 0) return CLOSE_OK;
    return  CLOSE_FAILED;
}

enum open_status open_file(
        const char *path,
        const char *modes,
        FILE** file
        ) {
    *file = fopen(path, modes);
    if (*file != NULL) return OPEN_OK;
    return OPEN_FAILED;
}
