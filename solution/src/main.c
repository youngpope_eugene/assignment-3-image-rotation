#include "bmp-util.h"
#include "error-handler.h"
#include "file.h"
#include "rotate.h"

int main(int argc, char **argv) {
    (void) argc; (void) argv;

    if (argc < 3) {
        err_print("Wrong number of arguments");
        return -1;
    }

    FILE *output_file = NULL;
    FILE *input_file = NULL;
    open_file(argv[1],"r", &input_file);
    open_file(argv[2],"w", &output_file);

    if (!input_file) {
        err_print("Bad first input file");
        return -1;
    }
    if (!output_file) {
        fclose(input_file);
        err_print("Bad second input file");
        return -1;
    }

    struct image image = {0};
    if (from_bmp( &image, input_file) == READ_ERROR) {
        err_print("FROM BMP ERROR!");
        close_file(&input_file);
        close_file(&output_file);
        return -1;
    }
    close_file(&input_file);

    struct image image_after_rotate = rotate(&image);

    int returnCode = -1;

    if (image_after_rotate.data != NULL) {
        if (to_bmp(&image_after_rotate, output_file) != WRITE_ERROR) {
            returnCode = 0;
        } else {
            err_print("TO BMP ERROR!");
        }
    } else {
        err_print("ROTATION ERROR. NOT ENOUGH MEMORY!");
    }

    close_file(&output_file);
    free_image(image);
    free_image(image_after_rotate);

    return returnCode;
}
