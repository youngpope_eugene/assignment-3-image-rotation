#ifndef IMAGEH
#define IMAGEH

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

struct image allocate_image(uint64_t width, uint64_t height);

void free_image(struct image img);

#endif
