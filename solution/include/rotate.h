#ifndef ROTATEH
#define ROTATEH

#include "image.h"

struct image rotate(struct image const* rotating_image);

#endif
