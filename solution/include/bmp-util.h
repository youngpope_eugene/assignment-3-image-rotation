#ifndef BMPH
#define BMPH

#include "image.h"

typedef struct __attribute__((packed)) {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmpHeader;

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 1,
    WRITE_OPERATION_ALLOCATION_ERROR = 2
};

enum read_status {
    READ_OK = 0,
    READ_ERROR = 1,
    READ_OPERATION_ALLOCATION_ERROR = 2,
    WRONG_FILE_READ_ERROR = 3
};

enum read_status from_bmp(struct image* img, FILE* file);

enum write_status to_bmp(struct image* img, FILE* file);

#endif
