#ifndef FILEH
#define FILEH

#include <stdio.h>

enum close_status {
    CLOSE_OK = 0,
    CLOSE_FAILED
};

enum open_status {
    OPEN_OK = 0,
    OPEN_FAILED
};

enum close_status close_file(FILE** file);

enum open_status open_file(const char *path, const char *modes, FILE **file);

#endif

